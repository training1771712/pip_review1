import 'dart:async';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'list_event.dart';
import 'list_state.dart';


class ApprovedListBloc {
  List<dynamic> approvedSecurities = [];
  int currentPage = 0;
  String selectedCategory = '';
  bool isLoading = false;
  final StreamController<ApprovedListState> _stateController = StreamController<ApprovedListState>.broadcast();
  TextEditingController searchController = TextEditingController();

  Stream<ApprovedListState> get stateStream => _stateController.stream;

  void mapEventToState(ApprovedListEvent event) async {
    if (event is FetchDataEvent) {
      await _fetchData(event.loanType, currentPage);
    } else if (event is LoadMoreDataEvent) {
      await _loadMoreData(event.loanType);
    }
  }

  void setSelectedCategory(String category, String loanType) {
    selectedCategory = category;
    List<dynamic> filteredSecurities = approvedSecurities.where((security) {
    String securityCategory = security['security_category_list'];
    return securityCategory == selectedCategory;
  }).toList();

  _stateController.add(DataLoadedState(filteredSecurities));
    _fetchData(loanType, currentPage);
  }

  Future<void> _fetchData(String loanType, int page) async {
    String loanTypeUrl = '';

    if (loanType == 'Equity') {
      loanTypeUrl = 'Equity';
    } else if (loanType == 'Mutual Fund - Equity') {
      loanTypeUrl = 'Mutual+Fund+-+Equity';
    } else if (loanType == 'Mutual Fund - Debt') {
      loanTypeUrl = 'Mutual+Fund+-+Debt';
    }

    final url = Uri.parse('https://uat-spark.atriina.com/api/method/lms.user.approved_securities?lender=Choice+Finserv&start=$page&per_page=20&search=${searchController.text}&category=$selectedCategory&is_download=0&loan_type=$loanTypeUrl');
    final headers = {
      'Authorization': 'token a467e37007eef6d:4982b3267717d43',
    };

    try {
      isLoading = true;
      _stateController.add(LoadingState());

      final response = await http.get(url, headers: headers);
      final jsonData = json.decode(response.body);

      final newApprovedSecurities = jsonData['data']['approved_securities_list'];
      if (page == 0) {
        approvedSecurities.clear();
      } 
      approvedSecurities.addAll(newApprovedSecurities);

      isLoading = false;
      _stateController.add(DataLoadedState(approvedSecurities));
      
      print('data fetched');
    } 
    catch (error) {
      print('Error fetching data: $error');
      isLoading = false;
      _stateController.add(ErrorState('Error fetching data'));
    }
  }

  Future<void> _loadMoreData(String loanType) async {
    if (!isLoading) {
      isLoading = true;
      currentPage += 20;
      await _fetchData(loanType, currentPage);
    }
  }

  void dispose() {
    _stateController.close();
  }
}
