abstract class ApprovedListEvent {}

class FetchDataEvent extends ApprovedListEvent {
  final String loanType;
  final String category;

  FetchDataEvent(this.loanType, this.category);
}

class LoadMoreDataEvent extends ApprovedListEvent {
  final String loanType;

  LoadMoreDataEvent(this.loanType);
}
