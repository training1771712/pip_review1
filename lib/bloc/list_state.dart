abstract class ApprovedListState {}

class InitialState extends ApprovedListState {
  
}

class LoadingState extends ApprovedListState {}

class DataLoadedState extends ApprovedListState {
  final List<dynamic> approvedSecurities;

  DataLoadedState(this.approvedSecurities);
}

class ErrorState extends ApprovedListState {
  final String errorMessage;

  ErrorState(this.errorMessage);
}
