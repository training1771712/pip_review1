import 'package:flutter/material.dart';
import 'bloc/list_bloc.dart';
import 'bloc/list_event.dart';
import 'bloc/list_state.dart';

class ApprovedListPage extends StatefulWidget {
  @override
  _ApprovedListPageState createState() => _ApprovedListPageState();
}

class _ApprovedListPageState extends State<ApprovedListPage> {
  final ApprovedListBloc _approvedListBloc = ApprovedListBloc();
  String selectedLoanType = 'Equity';
  bool isSearchBarVisible = false;
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _approvedListBloc.mapEventToState(FetchDataEvent('Equity', ''));
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _approvedListBloc.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  void _scrollListener() {
    if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
      _approvedListBloc.mapEventToState(LoadMoreDataEvent(selectedLoanType));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 50, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 50,
                  child: Row(
                    children: [
                      Expanded(
                        child: isSearchBarVisible
                            ? TextField(
                                controller: _approvedListBloc.searchController,
                                decoration: InputDecoration(
                                  hintText: 'Search',
                                  suffixIcon: IconButton(
                                    icon: const Icon(Icons.clear),
                                    onPressed: () {
                                        _approvedListBloc.searchController.clear();
                                        _approvedListBloc.mapEventToState(
                                          FetchDataEvent(selectedLoanType, ''),
                                        );
                                    },
                                  ),
                                ),
                                onChanged: (value) {
                                    _approvedListBloc.mapEventToState(
                                      FetchDataEvent(selectedLoanType, value),
                                    );
                                },
                              )
                            : Container(),
                      ),
                      const SizedBox(width: 10),
                      IconButton(
                        icon: const Icon(Icons.search),
                        onPressed: () {
                          setState(() {
                            isSearchBarVisible = !isSearchBarVisible;
                          });
                        },
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 20,),
                const Text(
                  'Approved Security',
                  style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: Colors.indigo),
                ),
                const SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    DropdownButton<String>(
                      value: selectedLoanType,
                      items: const [
                        DropdownMenuItem(
                          value: 'Equity',
                          child: Text('Equity'),
                        ),
                        DropdownMenuItem(
                          value: 'Mutual Fund - Equity',
                          child: Text('Mutual Fund - Equity'),
                        ),
                        DropdownMenuItem(
                          value: 'Mutual Fund - Debt',
                          child: Text('Mutual Fund - Debt'),
                        ),
                      ],
                      onChanged: (value) {
                        setState(() {
                          selectedLoanType = value!;
                          _approvedListBloc.mapEventToState(
                            FetchDataEvent(selectedLoanType, _approvedListBloc.searchController.text),
                          );
                        });
                      },
                    ),
                    const Spacer(),
                    IconButton(
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: const Text('Filter by Category'),
                              content: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  FilterChip(
                                    label: const Text('Category A'),
                                    selected: _approvedListBloc.selectedCategory == 'Category 1',
                                    onSelected: (bool isSelected) {
                                      _approvedListBloc.setSelectedCategory(isSelected ? 'Category 1' : '', selectedLoanType);
                                    },
                                  ),
                                  FilterChip(
                                    label: const Text('Category B'),
                                    selected: _approvedListBloc.selectedCategory == 'Category 2',
                                    onSelected: (bool isSelected) {
                                      _approvedListBloc.setSelectedCategory(isSelected ? 'Category 2' : '', selectedLoanType);
                                    },
                                  ),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
                _approvedListBloc.mapEventToState(FetchDataEvent(selectedLoanType, _approvedListBloc.searchController.text));
              },
              child: const Text('Apply'),
            ),
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('Cancel'),
            ),
          ],
        );
      },
    );
  },
  icon: const Icon(Icons.filter_alt),
),

                    IconButton(onPressed: (){}, icon: const Icon(Icons.picture_as_pdf))
                  ],
                ),
                //const SizedBox(height: 30,),
              ],
            ),
          ),
          StreamBuilder<ApprovedListState>(
            stream: _approvedListBloc.stateStream,
            initialData: InitialState(),
            builder: (context, snapshot) {
              final state = snapshot.data;
              if (state is InitialState) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state is LoadingState) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state is DataLoadedState) {
                final approvedSecurities = state.approvedSecurities;
                return Expanded(
                  child: ListView.builder(
                    controller: _scrollController,
                    itemCount: approvedSecurities.length + 1,
                    itemBuilder: (context, index) {
                      if (index < approvedSecurities.length) {
                      return Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey, width: 1.0),
                        ),
                        margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 15),
                        child: ListTile(
                          title: Text('${approvedSecurities[index]['security_name']}' ,style: const TextStyle(fontSize: 19, fontWeight: FontWeight.bold, color: Colors.indigo),),
                          subtitle: Column(
                            children: [
                              const SizedBox(height: 6,),
                              Row(
                                children: [
                                  const Icon(Icons.check_circle, color: Colors.green,size: 15,),
                                  const SizedBox(width: 5,),
                                  Text('${approvedSecurities[index]['security_category']} (LTV : ${approvedSecurities[index]['eligible_percentage']}%)',
                                  style: const TextStyle(fontSize: 16, color: Colors.green, fontWeight: FontWeight.bold),),
                                ],
                              ),
                            ],
                          )
                        ),
                      );
                    }
                    else if (_approvedListBloc.isLoading) {
                    // Render the loading indicator if more data is being loaded
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    } 
                    else {
                      // Render an empty container
                      return Container();
                    }
                    }
                  ),
                );
              } else if (state is ErrorState) {
                return Center(
                  child: Text(state.errorMessage),
                );
              }
              return Container();
            },
          ),
        ],
      ),
    );
  }
}